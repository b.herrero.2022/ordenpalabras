import sys
def is_lower(first: str, second: str):
    x= first.lower()
    y= second.lower()
    if x < y:
        return True
    else:
        return False

def get_lower(words: list, pos: int):
    for z in range(pos, len(words)):
        if not is_lower(words [pos], words[z]):
            pos = z
        return pos

def sort(words: list):
    Lista = ['']
    for a in range (0,len (words)):
        numbers=get_lower(words, a)
        Lista+= [words[numbers]]
        words[numbers]=words[a]
    return Lista

def show(words: list ):
    return print(words)

def main():
    words:list = sys.argv[1:]
    ordered: list = sort(words)
    show(ordered)

if __name__ =='__main__':
    main()
